using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

class Pod
{
    public Point2D Position;
    public Point2D PredictedPosition
    {
        get { return Position + Velocity; }
    }
    public Vector2D Velocity;
    public double VelocityAbs
    {
        get { return Velocity.Length(); }
    }
    public int Angle;
    public int ActiveCP;

    public Pod(Point2D pos, Vector2D velocity, int angle, int activeCheckPoint)
    {
        Position = pos;
        Velocity = velocity;
        Angle = angle;
        ActiveCP = activeCheckPoint;
    }
    public void Move(int x, int y, int acceleration)
    {
        Console.WriteLine("{0} {1} {2}", x, y, acceleration);
    }
    public void Move(Point2D p, int acceleration)
    {
        Move((int)p.X, (int)p.Y, acceleration);
    }

    public void Shield()
    {
        Console.WriteLine("Shield");
    }
}

class Vector2D
{
    public double X, Y;

    public Vector2D(double x, double y)
    {
        X = x;
        Y = y;
    }

    public void Normalize()
    {
        double length = Length();
        //Console.Error.WriteLine(length);
        X = (X / length);
        Y = (Y / length);
    }

    public static Vector2D operator /(Vector2D v, double i)
    {
        return new Vector2D(v.X / i, v.Y / i);
    }
    public static Vector2D operator *(Vector2D v, double i)
    {
        return new Vector2D(v.X * i, v.Y * i);
    }

    public double LengthSqr()
    {
        return X * X + Y * Y;
    }

    public double Length()
    {
        return Math.Sqrt(LengthSqr());
    }
}

class Point2D
{
    public double X, Y;

    public Point2D(double x, double y)
    {
        X = x;
        Y = y;
    }

    public static Vector2D operator -(Point2D p1, Point2D p2)
    {
        return new Vector2D(p1.X - p2.X, p1.Y - p2.Y); // Stop fucking with coordinates codingame
    }

    public static Point2D operator +(Point2D p1, Vector2D v)
    {
        return new Point2D(p1.X + v.X, p1.Y + v.Y);
    }

    public double Distance(Point2D p)
    {
        return (p - this).Length();
    }
}

class Player
{
    static void Main(string[] args)
    {
        string[] inputs;
        int laps = int.Parse(Console.ReadLine());
        int checkpointCount = int.Parse(Console.ReadLine());
        Console.Error.WriteLine(checkpointCount);
        Point2D[] checkPoints = new Point2D[checkpointCount + 1];
        for (int i = 0; i < checkpointCount; i++)
        {
            inputs = Console.ReadLine().Split(' ');
            checkPoints[i] = new Point2D(int.Parse(inputs[0]), int.Parse(inputs[1]));
            Console.Error.WriteLine("Added checkpoint: " + i);
        }

        // game loop
        while (true)
        {
            Pod[] pods = new Pod[4];
            for (int i = 0; i < 4; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                Point2D pos = new Point2D(int.Parse(inputs[0]), int.Parse(inputs[1]));
                Vector2D velocity = new Vector2D(int.Parse(inputs[2]), int.Parse(inputs[3]));
                int angle = int.Parse(inputs[4]); // What to do with dis
                int nextCheckPointId = int.Parse(inputs[5]);

                pods[i] = new Pod(pos, velocity, angle, nextCheckPointId);
            }
            //My Pods
            /*for (int i = 0; i < 2; i++)
            {
                Point2D pos = new Point2D(int.Parse(inputs[0]), int.Parse(inputs[1]));
                Vector2D velocity = new Vector2D(int.Parse(inputs[2]), int.Parse(inputs[3]));
                Console.Error.WriteLine("Velocity {0} u/s", velocity.Length());

                int angle = int.Parse(inputs[4]); // What to do with dis
                Console.Error.WriteLine("Angle: " + angle);
                int nextCheckPointId = int.Parse(inputs[5]);

                var checkPoint = checkPoints[nextCheckPointId];
                Console.Error.WriteLine(nextCheckPointId);

                var checkPointN = nextCheckPointId + 1 == nextCheckPointId ?
                    null : checkPoints[nextCheckPointId + 1];

                if (checkPointN == null)
                {
                    checkPointN = checkPoints[0];
                }
                var leetVector = (checkPointN - checkPoint);
                leetVector.Normalize();
                leetVector *= 500;

                checkPoint += leetVector;
                var predictedPos = pos + velocity;
                if (velocity.Length() <= 50)
                {
                    Console.WriteLine("{0} {1} {2}", (int)checkPoint.X, (int)checkPoint.Y, 100); continue;
                }
                else if (velocity.Length() > 450)
                {
                    Console.WriteLine("{0} {1} {2}", (int)checkPoint.X, (int)checkPoint.Y, 0); continue;
                }
                if (predictedPos.Distance(checkPoint) < 500)
                {
                    Console.WriteLine("{0} {1} {2}", (int) checkPointN.X, (int) checkPointN.Y, 10); continue;
                }
                Console.WriteLine("{0} {1} {2}", (int)checkPoint.X, (int)checkPoint.Y,
                    predictedPos.Distance(checkPoint) > 700 ? 100 : 20);



            }
            //EnemyPods
            for (int i = 0; i < 2; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                int x = int.Parse(inputs[0]);
                int y = int.Parse(inputs[1]);
                int vx = int.Parse(inputs[2]);
                int vy = int.Parse(inputs[3]);
                int angle = int.Parse(inputs[4]);
                int nextCheckPointId = int.Parse(inputs[5]);
            }*/

            // Write an action using Console.WriteLine()
            // To debug: Console.Error.WriteLine("Debug messages...");

        }
    }
}